package pages;

import org.openqa.selenium.support.PageFactory;

import cucumber.api.java.en.And;
import wdMethods.ProjectMethods;

public class CloseBrowserPage extends ProjectMethods{
	
	public CloseBrowserPage() {
		PageFactory.initElements(driver, this);
	}
@And("Close Browser")
public CloseBrowserPage Close() {
	driver.close();
	return this ;
}
	
}
