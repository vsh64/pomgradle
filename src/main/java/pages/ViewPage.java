package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class ViewPage extends ProjectMethods{

	public ViewPage() {
		PageFactory.initElements(driver, this);
	}
@FindBy(id="viewLead_companyName_sp") WebElement VerCompNm;
@FindBy(id="viewLead_firstName_sp") WebElement VerFstNm;
@FindBy(id="viewLead_lastName_sp") WebElement VerlstNm;

	public ViewPage VerifyCompanyName(String CompName) {
		
		//WebElement VerCompNm = locateElement("id", "viewLead_companyName_sp");
		verifyPartialText(VerCompNm, CompName);
		System.out.println("Comp name Verified");

		return this;
	}
public ViewPage VerifyFirstName(String FirstName) {
		
	//	WebElement VerFstNm = locateElement("id", "viewLead_firstName_sp");
		verifyExactText(VerFstNm, FirstName);
		System.out.println("First name Verified");
		return this;
	}
public CloseBrowserPage VerifyLastName(String LastName) {
		
	//	WebElement VerlstNm = locateElement("id", "viewLead_lastName_sp");
		verifyExactText(VerlstNm, LastName);
		System.out.println("Last name Verified");
		return new CloseBrowserPage();
	}
}
