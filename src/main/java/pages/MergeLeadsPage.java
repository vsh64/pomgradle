package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class MergeLeadsPage extends ProjectMethods{

	public MergeLeadsPage() {
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(id="ComboBox_partyIdFrom") WebElement eleFromLead;
	public MergeLeadsPage enterFromLead(String FromLeadID) {
		type(eleFromLead,FromLeadID);
		return this ;
	}
	
	@FindBy(id="ComboBox_partyIdTo") WebElement eleToLead;
	public MergeLeadsPage enterToLead(String ToLeadID) {
		type(eleToLead, ToLeadID);
		return this ;
	}
	@FindBy(linkText="Merge") WebElement eleMerge;
	public MyLeadsPage clickMerge() {
		click(eleMerge);
		acceptAlert();
		return new MyLeadsPage();
	}
}
