package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import cucumber.api.java.en.And;
import cucumber.api.java.en.When;
import wdMethods.ProjectMethods;

public class CreateLeadPage extends ProjectMethods{

public CreateLeadPage() {
	PageFactory.initElements(driver, this);
}


@FindBy(id="createLeadForm_companyName") WebElement eleCompName;
@FindBy(id="createLeadForm_firstName") WebElement eleFirstName;
@FindBy(id="createLeadForm_lastName") WebElement eleLastName;
@FindBy(name="submitButton") WebElement eleCreateLead;

@And("Enter Company Name as (.*)")
public CreateLeadPage enterCompanyName(String CompName) {
	
	//WebElement eleCompName = locateElement("id", "createLeadForm_companyName");
	type(eleCompName, CompName);
	return this;
} 
@And("Enter FirstName as (.*)")
public CreateLeadPage enterFirstName(String FirstName) {
	
	//WebElement eleFirstName = locateElement("id", "createLeadForm_firstName");
	type(eleFirstName, FirstName);
	return this;
}
@And("Enter LastName as (.*)")
public CreateLeadPage enterLastName(String LastName) {
	
	//WebElement eleLastName = locateElement("id", "createLeadForm_lastName");
	type(eleLastName, LastName);
	return this;
}
@When("Click on CreateLead button")
public ViewPage ClickToCreateLead() {
	
	//WebElement eleCreateLead = locateElement("name", "submitButton");
	click(eleCreateLead);
	return new ViewPage();
}


}
