package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import cucumber.api.java.en.And;
import wdMethods.ProjectMethods;

public class MyLeadsPage extends ProjectMethods{
	
	public MyLeadsPage() {
		
		PageFactory.initElements(driver, this);
	}
@FindBy(linkText="Create Lead") WebElement eleClickLeadCreate;
@And("Click on CreateLead")
	public CreateLeadPage ClickCreateLead() {
		//WebElement eleClickLeadCreate = locateElement("link", "Create Lead");
		click(eleClickLeadCreate);
		
		return new CreateLeadPage();
		
	}
	@FindBy(linkText="Merge Leads") WebElement eleClickMergeLeads;
	public MergeLeadsPage ClickMergeLeads() {
		click(eleClickMergeLeads);
		return new MergeLeadsPage();
	}
	@FindBy(linkText="Find Leads") WebElement eleClickFindLeads;
	public FindLeadsPage clickFindLeads() {
		click(eleClickFindLeads);
		return new FindLeadsPage();
	}
}
