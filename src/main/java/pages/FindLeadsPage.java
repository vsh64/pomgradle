package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class FindLeadsPage extends ProjectMethods{

	public FindLeadsPage() {
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(name="id") WebElement eleLeadID;
	public FindLeadsPage enterLeadId(String FromLeadID) {
		type(eleLeadID, FromLeadID);
		return this;
		
	}
	@FindBy(xpath="//button[text()='Find Leads']") WebElement eleFindLead;
	public FindLeadsPage clickFindLead() {
		click(eleFindLead);	
		return this;
	}
	@FindBy(xpath="//div[text()='No records to display']") WebElement eleLeadRecordFound;
	public FindLeadsPage NoLeadsRecordsFound() {
		verifyExactText(eleLeadRecordFound, "No records to display");
		return this;
	}
	
}
