package testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import pages.LoginPage;
import wdMethods.ProjectMethods;

public class TC_002_CreateLead extends ProjectMethods{
	
	@BeforeTest
	public void setUp() {
		testCaseName="TC_002_CreateLead";
		testDescription="Creating a Lead";
		authors="vishnu";
		category="smoke";
		testNodes="Leads";
		dataSheetName="TC001";
		
	}
	
	@Test(dataProvider="fetchData" )
	public void CreateLead(String userName,String password,String CompName ,String FirstName,String LastName) {
		
		new LoginPage().enterUserName(userName).enterPassword(password)
		.clickLogin().ClickCRMSFA().ClickLeads().ClickCreateLead().enterCompanyName(CompName).enterFirstName(FirstName)
		.enterLastName(LastName).ClickToCreateLead().VerifyCompanyName(CompName)
		.VerifyFirstName(FirstName).VerifyLastName(LastName).Close();
		
	}
	
	
	
	

}
