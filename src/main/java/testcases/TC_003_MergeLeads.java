package testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import pages.LoginPage;
import wdMethods.ProjectMethods;

public class TC_003_MergeLeads extends ProjectMethods {

	@BeforeTest
	public void setUp() {
		testCaseName="TC_003_MergeLeads";
		testDescription="Merge  Leads";
		authors="vishnu";
		category="smoke";
		testNodes="MergeLeads";
		dataSheetName="TC003";
		
	}
	 
	@Test(dataProvider="fetchData" )
	public void CreateLead(String userName,String password,String FromLeadID ,String ToLeadID) {
		
		new LoginPage().enterUserName(userName).enterPassword(password)
		.clickLogin().ClickCRMSFA().ClickLeads().ClickMergeLeads().enterFromLead(FromLeadID)
		.enterToLead(ToLeadID).clickMerge().clickFindLeads().enterLeadId(FromLeadID).clickFindLead().NoLeadsRecordsFound();
		
	}
	
}
