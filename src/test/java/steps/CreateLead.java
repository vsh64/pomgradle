//package steps;
//
//import java.util.concurrent.TimeUnit;
//
//import org.openqa.selenium.chrome.ChromeDriver;
//
//import cucumber.api.PendingException;
//import cucumber.api.java.en.And;
//import cucumber.api.java.en.Given;
//import cucumber.api.java.en.Then;
//import cucumber.api.java.en.When;
//
//public class CreateLead {
//
//	ChromeDriver driver;
//
//	@Given("Open the Browser")
//	public void OpenBrowser() {
//		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
//		driver = new ChromeDriver();
//	}
//
//	@And("Max the Browser")
//	public void MaxBrowser() {
//
//		driver.manage().window().maximize();
//	}
//
//	@And("Set TimeOut")
//	public void setTimeOut() {
//		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
//	}
//
//	@And("Launch the URL")
//	public void Url() {
//		driver.get("http://leaftaps.com/opentaps");
//	}
//
//	@And("Enter Username as (.*)")
//	public void enterUserName(String username) {
//		driver.findElementById("username").sendKeys(username);
//	}
//
//	@And("Enter Password as (.*)")
//	public void enterPassword(String password) {
//		driver.findElementById("password").sendKeys(password);
//	}
//
//	@When("Click on Login")
//	public void clickLogin() {
//		driver.findElementByClassName("decorativeSubmit").click();
//	}
//
//	@Then("Verify Login")
//	public void VerifyLogin() {
//		System.out.println("Success");
//	}
//
//	@Then("Click CRMSFA")
//	public void click_CRMSFA() {
//
//		driver.findElementByLinkText("CRM/SFA").click();
//	}
//
//	@Then("Click Leads")
//	public void click_Leads() {
//
//	}
//
//	@Then("Click on CreateLead")
//	public void click_on_CreateLead() {
//		driver.findElementByLinkText("Create Lead").click();
//
//	}
//
//	@Then("Enter Company Name as (.*)")
//	public void enter_Company_Name(String CName) {
//		driver.findElementById("createLeadForm_companyName").sendKeys(CName);
//
//	}
//
//	@Then("Enter FirstName as (.*)")
//	public void enter_FirstName(String Fname) {
//		driver.findElementById("createLeadForm_firstName").sendKeys(Fname);
//	}
//
//	@Then("Enter LastName as (.*)")
//	public void enter_LastName(String Lname) {
//		driver.findElementById("createLeadForm_lastName").sendKeys(Lname);
//	}
//
//	@When("Click on CreateLead button")
//	public void click_on_CreateLead_button() {
//		driver.findElementByName("submitButton").click();
//	}
//	@Then("Close Browser")
//	public void Close() {
//		driver.quit();
//	}
//	
//
//}
