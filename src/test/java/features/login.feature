Feature: Login to Leaftaps

  #Background: 
    #Given Open the Browser
    #And Max the Browser
    #And Set TimeOut
    #And Launch the URL

  Scenario Outline: Positive Login 1
   
    And Enter Username as <username>
    And Enter Password as <password>
    When Click on Login
    #Then Verify Login
    And Click CRMSFA
    And Click Leads
    And Click on CreateLead
    And Enter Company Name as <CName>
    And Enter FirstName as <Fname>
    And Enter LastName as <Lname>
    When Click on CreateLead button 
    Then Close Browser

   Examples: 
      | username         | password |CName|Fname|Lname|
      | DemoSalesManager | crmsfa   |IBM|Vsh|Vsh1|
      | DemoSalesManager | crmsfa   |CTS|Vig|Kik|
      | DemoSalesManager | crmsfa   |Infosys|Vsh|Vin|
      
  Scenario Outline: Positive Login 2
  
    And Enter Username as <username>
    And Enter Password as <password>
    When Click on Login
    #Then Verify Login
    And Click CRMSFA
    And Click Leads
    And Click on CreateLead
    And Enter Company Name as <CName>
    And Enter FirstName as <Fname>
    And Enter LastName as <Lname>
    When Click on CreateLead button 
    Then Close Browser

    Examples: 
      | username         | password |CName|Fname|Lname|
      | DemoSalesManager | crmsfa   |IBM|Vsh|Vsh1|
      | DemoSalesManager | crmsfa   |CTS|Vig|Kik|
      | DemoSalesManager | crmsfa   |Infosys|Vsh|Vin|
      
