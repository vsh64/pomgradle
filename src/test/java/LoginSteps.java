

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.chrome.ChromeDriver;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class LoginSteps {
	
	ChromeDriver driver;
	@Given("Open the Browser")
	public void OpenBrowser() {
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		driver = new ChromeDriver();
	}
	@And("Max the Browser")
	public void MaxBrowser() {
		
		driver.manage().window().maximize();
	}
	@And("Set TimeOut")
	public void setTimeOut() {
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
	}
	@And("Launch the URL")
	public void Url() {
		driver.get("http://leaftaps.com/opentaps");
	}
	@And("Enter Username as (.*)")
	public void enterUserName(String username) {
		driver.findElementById("username").sendKeys(username);
	}
	@And("Enter Password as (.*)")
	public void enterPassword(String password) {
		driver.findElementById("password").sendKeys(password);
	}
	@When("Click on Login")
	public void clickLogin() {
		driver.findElementByClassName("decorativeSubmit").click();
	}
	@Then("Verify Login")
	public void VerifyLogin() {
		System.out.println("Success");
	}
}
